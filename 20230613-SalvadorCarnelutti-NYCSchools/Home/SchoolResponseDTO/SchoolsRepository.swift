//
//  SchoolsRepository.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//

import Foundation

protocol SchoolsRepositoryProtocol {
    func getSchools(onCompletion completionHandler: @escaping (Result<[School], Error>) -> Void)
    func loadScoresFor(dbn: String, onCompletion completionHandler: @escaping (Result<[Score], Error>) -> Void)
}

class SchoolsRepository: SchoolsRepositoryProtocol {
    func getSchools(onCompletion completionHandler: @escaping (Result<[School], Error>) -> Void) {
        let charactersUrl: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=40"
        let headers: [String: String] = [
            "$$app_token": "FmDGD0UhbLKav3wK3srH2gpac",
        ]
        
        RestClient<[School]>(httpManager: HTTPManager()).get(charactersUrl, params: nil, headers: headers) { result in
            switch result {
            case .success(let schoolsDTO):
                completionHandler(.success(schoolsDTO))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    func loadScoresFor(dbn: String, onCompletion completionHandler: @escaping (Result<[Score], Error>) -> Void) {
        let comicsUrl: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        let headers = [
            "$$app_token": "FmDGD0UhbLKav3wK3srH2gpac",
        ]

        RestClient<[Score]>(httpManager: HTTPManager()).get(comicsUrl, params: nil, headers: headers) { result in
            switch result {
            case .success(let scoresDTO):
                completionHandler(.success(scoresDTO))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}
