//
//  SchoolsResponseDTO.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//

import Foundation

struct School: Codable {
    let dbn: String
    let schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
    }
}

struct Score: Codable {
    let schoolName: String
    let satCriticalReadingAverage: String
    let satMathAverage: String
    let satWritingAverage: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case satCriticalReadingAverage = "sat_critical_reading_avg_score"
        case satMathAverage = "sat_math_avg_score"
        case satWritingAverage = "sat_writing_avg_score"
    }
    
    var scoreAverages: [ScoreAverageModel] {
        [ScoreAverageModel(scoreKind: "Critical reading average", score: satCriticalReadingAverage),
         ScoreAverageModel(scoreKind: "Math average", score: satMathAverage),
         ScoreAverageModel(scoreKind: "Writing average", score: satWritingAverage)]
    }
}
