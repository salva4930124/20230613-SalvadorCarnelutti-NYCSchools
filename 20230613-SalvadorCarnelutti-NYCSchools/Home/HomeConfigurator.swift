
//  
//  HomeConfigurator.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//
//
import Foundation

final class HomeConfigurator {
    static func injectDependencies(view: HomePresenterToViewProtocol,
                                   interactor: HomePresenterToInteractorProtocol,
                                   presenter: HomePresenter,
                                   router: HomeRouter) {
        presenter.interactor = interactor
        interactor.presenter = presenter

        view.presenter = presenter
        presenter.viewHome = view
        
        router.presenter = presenter
        presenter.router = router
    }
    
    static func resolve() -> HomePresenter {
        let presenter = HomePresenter()
        let view = HomeView()
        let interactor = HomeInteractor(schoolsRepository: SchoolsRepository())
        let router = HomeRouter()

        Self.injectDependencies(view: view,
                                interactor: interactor,
                                presenter: presenter,
                                router: router)

        return presenter
    }
}
