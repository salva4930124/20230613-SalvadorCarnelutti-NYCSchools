//
//  UITableView.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//

import UIKit

extension UITableView {
    func register(_ cellClass: UITableViewCell.Type) {
        register(cellClass, forCellReuseIdentifier: cellClass.identifier)
    }
    
    func register(_ headerFooterClass: UITableViewHeaderFooterView.Type) {
        register(headerFooterClass, forHeaderFooterViewReuseIdentifier: headerFooterClass.identifier)
    }
}
