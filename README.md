## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Design pattern chosen](#design-pattern-chosen)
* [Setup](#setup)
* [Improvements](#improvements)
* [Known issues](#known-issues)

## General info

This project is a NYC's schools SAT scores browser, done in about a 8 hour span, with the capabilities to:
- Browse over a series of NYC schools
- Get additional info on those school's SAT scores
	
## Technologies
Project is created with:
* VIPER design pattern
* CocoaPods dependency manager
* Alamofire framework
* Codable protocol
* The NYC OpenData free public REST API (https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2)

## Design pattern chosen
VIPER was chosen as an optimal way to modularize and encapsulate properly responsibilities for each screen, this allows for better decoupling and this as well makes it easier to debug and test features.
	
## Setup
To run this project, install the required frameworks using CocoaPods:

```
$ cd ../20230613-SalvadorCarnelutti-NYCSchools
$ pod install
```

## Improvements
Because of of the shortage of time given, the app could benefit from a prefetching table view implementation (Infinite scrolling) and a search behaviour if the students know which school to look for.

## Known issues
I only list 40 of the total schools given I didn't do the infinite scrolling behaviour and loading all schools was not efficient.
