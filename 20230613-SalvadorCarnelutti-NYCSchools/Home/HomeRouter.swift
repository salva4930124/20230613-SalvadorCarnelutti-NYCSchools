////
////  
//  HomeRouter.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//
////
import UIKit

protocol HomePresenterToRouterProtocol: AnyObject {
    var presenter: HomeRouterToPresenterProtocol? { get set }
    func presentScores(scores: ScoresModel)
}

// MARK: - PresenterToInteractorProtocol
final class HomeRouter: HomePresenterToRouterProtocol {
    // MARK: - Properties
    weak var presenter: HomeRouterToPresenterProtocol?
    
    func presentScores(scores: ScoresModel) {
        let viewController = ScoresTableViewController(scores: scores)
        viewController.modalPresentationStyle = .popover
        presenter?.present(viewController, animated: true)
    }
}
