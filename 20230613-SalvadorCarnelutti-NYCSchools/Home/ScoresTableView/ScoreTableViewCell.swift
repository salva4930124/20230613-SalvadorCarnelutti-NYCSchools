//
//  ScoreTableViewCell.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/14/23.
//

import UIKit

final class ScoreTableViewCell: UITableViewCell {
    private lazy var headingLabel: UILabel = {
        let label = UILabel()
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 24, weight: .semibold)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    private lazy var scoreLabel: UILabel = {
        let label = UILabel()
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            headingLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12),
            headingLabel.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 16),
            headingLabel.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -12)
        ])
        
        NSLayoutConstraint.activate([
            scoreLabel.leftAnchor.constraint(equalTo: headingLabel.rightAnchor, constant: 8),
            scoreLabel.rightAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.rightAnchor, constant: 16),
            scoreLabel.bottomAnchor.constraint(equalTo: headingLabel.bottomAnchor)
        ])
    }
    
    func configure(with scoreModel: ScoreAverageModel) {
        headingLabel.text = "\(scoreModel.scoreKind):"
        scoreLabel.text = scoreModel.score
    }
}
