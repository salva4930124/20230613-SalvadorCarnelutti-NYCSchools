//
//  ScoresTableViewController.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/14/23.
//

struct ScoresModel {
    let schoolName: String
    let scoreAverages: [ScoreAverageModel]
}

struct ScoreAverageModel {
    let scoreKind: String
    let score: String
}

import UIKit

class ScoresTableViewController: UITableViewController {
    private let scoreAverages: [ScoreAverageModel]
    private let schoolName: String
    
    init(scores: ScoresModel) {
        self.scoreAverages = scores.scoreAverages
        self.schoolName = scores.schoolName
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ScoreTableViewCell.self)
        tableView.register(ScoresTableViewHeader.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .white
        tableView.clipsToBounds = false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreAverages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ScoreTableViewCell.identifier, for: indexPath) as? ScoreTableViewCell else {
            ScoreTableViewCell.assertCellFailure()
            return UITableViewCell()
        }
        
        cell.configure(with: scoreAverages[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(
                withIdentifier: ScoresTableViewHeader.identifier) as? ScoresTableViewHeader else {
            ScoresTableViewHeader.assertHeaderFailure()
            return UITableViewHeaderFooterView()
        }
        
        header.configure(with: "\(schoolName.capitalized)'\(schoolName.last?.lowercased() != "s" ? "s" : "") SAT scores")
        return header
    }
}
