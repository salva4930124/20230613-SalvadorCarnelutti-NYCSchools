//
//  
//  HomeInteractor.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//
//

enum CustomError: Error {
    case emptyScores
    
    var errorDescription: String {
        return NSLocalizedString("There are no vailable SAT scores registered for this school", comment: "")
    }
}

import Foundation

protocol HomePresenterToInteractorProtocol: AnyObject {
    var presenter: BaseViewProtocol? { get set }
    func loadSchools(onCompletion completionHandler: @escaping (Result<[School], Error>) -> ())
    func loadScoresFor(dbn: String, onCompletion completionHandler: @escaping (Result<Score, Error>) -> Void)
}

// MARK: - PresenterToInteractorProtocol
final class HomeInteractor: HomePresenterToInteractorProtocol {
    private let schoolsRepository: SchoolsRepositoryProtocol
    weak var presenter: BaseViewProtocol?
    
    init(schoolsRepository: SchoolsRepositoryProtocol) {
        self.schoolsRepository = schoolsRepository
    }
    
    func loadSchools(onCompletion completionHandler: @escaping (Result<[School], Error>) -> ()) {
        presenter?.showLoader()
        schoolsRepository.getSchools { [weak self] result in
            guard let self = self else { return }
            self.presenter?.hideLoader()
            
            switch result {
            case .success(let schoolsResponse):
                let schools = schoolsResponse
                completionHandler(.success(schools))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
    
    func loadScoresFor(dbn: String, onCompletion completionHandler: @escaping (Result<Score, Error>) -> Void) {
        presenter?.showLoader()
        schoolsRepository.loadScoresFor(dbn: dbn) { [weak self] result in
            guard let self = self else { return }
            self.presenter?.hideLoader()
            
            switch result {
            case .success(let scoresResponse) where !scoresResponse.isEmpty:
                completionHandler(.success(scoresResponse.first!))
            case .success(_):
                completionHandler(.failure(CustomError.emptyScores))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}
