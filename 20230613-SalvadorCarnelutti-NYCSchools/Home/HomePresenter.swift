//
//  
//  HomePresenter.swift
//  20230613-SalvadorCarnelutti-NYCSchools
//
//  Created by Salvador on 6/13/23.
//
//

import UIKit

protocol HomeViewToPresenterProtocol: UIViewController {
    var itemsCount: Int { get }
    func viewLoaded()
    func itemAt(row: Int) -> String
    func didSelectSchoolAt(row: Int)
}

protocol HomeRouterToPresenterProtocol: UIViewController {}

final class HomePresenter: BaseViewController {
    private var schools = [School]()
    var viewHome: HomePresenterToViewProtocol!
    var interactor: HomePresenterToInteractorProtocol!
    var router: HomePresenterToRouterProtocol!
    
    override func loadView() {
        super.loadView()
        view = viewHome
        viewHome.loadView()
        loadSchools()
        title = "NYCSchools"
    }
    
    private func loadSchools() {
        interactor.loadSchools { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let schools):
                self.schools = schools
                self.viewHome.reloadData()
            case .failure(let error):
                self.presentOKAlert(title: "Schools load error", message: error.localizedDescription)
            }
        }
    }
}

// MARK: - ViewToPresenterProtocol
extension HomePresenter: HomeViewToPresenterProtocol {
    var itemsCount: Int {
        schools.count
    }
    
    func itemAt(row: Int) -> String {
        schools[row].schoolName
    }
    
    func didSelectSchoolAt(row: Int) {
        interactor.loadScoresFor(dbn: schools[row].dbn) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let score):
                let scoresModel = ScoresModel(schoolName: score.schoolName, scoreAverages: score.scoreAverages)
                self.router.presentScores(scores: scoresModel)
            case .failure(let error):
                if let customError = error as? CustomError {
                    self.presentOKAlert(title: "Scores load error", message: customError.errorDescription)
                } else {
                    self.presentOKAlert(title: "Scores load error", message: error.localizedDescription)
                }
                
            }
        }
    }
    
    func viewLoaded() {}
}

// MARK: - RouterToPresenterProtocol
extension HomePresenter: HomeRouterToPresenterProtocol {}
